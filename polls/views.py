from django.shortcuts import render, reverse
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic

from .models import Question

def index (request):
  return HttpResponse("Hello, i am here.")

def questions(request):
  allQ = Question.objects.all()
  context = {
    "questions" : allQ
  }

  return render(request, "questions.html", context=context )

def questionSingle(request, questionID):
  single = Question.objects.get(pk=questionID)
  context = {
    "question": single
  }
  return render(request, "single-question.html", context=context)

def formQuestionCreateTemplate(request):
  return render(request, 'create-question.html')

def formQuestionCreate(request):
  question_text = request.POST["question_text"]
  question_date = timezone.now()
  newQuestion = Question(question_text=question_text, question_date=question_date)

  newQuestion.save()

  return HttpResponseRedirect(reverse("polls:questions"))

  
  
