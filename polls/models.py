from django.db import models


class Question(models.Model):

  def __str__(self):
    return self.question_text

  question_text = models.CharField(max_length=200)
  question_date = models.DateTimeField("Date Published")
  



class Choice(models.Model):


  def __str__(self):
    return self.choice_text

  question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
  choice_text = models.CharField(max_length=200)
  choice_date = models.DateTimeField("Choice published")
