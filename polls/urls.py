from django.urls import path
from . import views

app_name = "polls"

urlpatterns = [
  path("", views.index, name='index'),
  path("questions", views.questions, name='questions'),
  path("questions/<int:questionID>", views.questionSingle, name='single_question'),
  path("questions/create", views.formQuestionCreateTemplate, name='create_question'),
  path('questions/create_post', views.formQuestionCreate, name='create_question_post')
]
